<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Cardset;
use App\Player;

final class PlayerTest extends TestCase
{
    public function testRemainDeck()
    {
        $cardSet = new Cardset();

        $cardSet->shuffle();

        $player1 = new Player();
        $player2 = new Player();
        $player3 = new Player();
        $player4 = new Player();

        $remainDeck = $cardSet->divideCards([$player1, $player2, $player3, $player4], 7);

        $p1Cards = $player1->getDeck();

        //test player takes a card from remain deck
        $remainDeckAfter = $player1->getCardfromRemainDeck($remainDeck);
        $p1CardsAfter = $player1->getDeck();

        // remain deck should reduced by 1 card
        $this->assertEquals(count($remainDeckAfter), count($remainDeck) - 1);

        // players deck should increase by 1 card
        $this->assertEquals(count($p1CardsAfter), count($p1Cards) + 1);
    }

    public function testResetStrength()
    {
        $cardSet = new Cardset();
        $player1 = new Player();

        $card1 = [
            'suite' => 'hearts',
            'color' => 'red',
            'symbol' => '\U2665',
            'face' => '2',
            'strength' => 2
        ];
        $card2 = [
            'suite' => 'hearts',
            'color' => 'red',
            'symbol' => '\U2665',
            'face' => '3',
            'strength' => 1
        ];
        

        $resettedCards = $player1->resetCardStrength([$card1, $card2]);

        $this->assertEquals(0, $resettedCards[0]['strength']);
    }

    public function testUpdateCardStrength()
    {
        $cardSet = new Cardset();
        $player1 = new Player();

        $card1 = [
            'suite' => 'hearts',
            'color' => 'red',
            'symbol' => '\U2665',
            'face' => '2',
            'strength' => 0
        ];
        $card2 = [
            'suite' => 'clubs',
            'color' => 'black',
            'symbol' => '\U2663',
            'face' => '3',
            'strength' => 0
        ];

        $card3 = [
            'suite' => 'hearts',
            'color' => 'red',
            'symbol' => '\U2665',
            'face' => '3',
            'strength' => 0
        ];
        $card4 = [
            'suite' => 'clubs',
            'color' => 'black',
            'symbol' => '\U2663',
            'face' => '2',
            'strength' => 0
        ];

        $remainDeck = [$card1, $card2];
        $playerDeck = [$card3, $card4];

        $cardSet->setRemainDeck($remainDeck);
        $exposedCard = $cardSet->flipCard();
        $cards = $player1->updateCardStrength($playerDeck, $exposedCard);

        $this->assertEquals(2, $cards[0]['strength']);
        $this->assertEquals(1, $cards[1]['strength']);

        $remainDeck = [$card2, $card1];
        $playerDeck = [$card3, $card4];
        
        $exposedCard = $cardSet->flipCard();
        $cards = $player1->updateCardStrength($playerDeck, $exposedCard);
        $this->assertEquals(1, $cards[0]['strength']);
    }

    public function testplayCard()
    {
        $cardSet = new Cardset();
        $player1 = new Player();

        $cards =[
            [
                'suite' => 'hearts',
                'color' => 'red',
                'symbol' => '\U2665',
                'face' => '2',
                'strength' => 0
            ],
            [
                'suite' => 'clubs',
                'color' => 'black',
                'symbol' => '\U2663',
                'face' => '3',
                'strength' => 0
            ],
            [
                'suite' => 'clubs',
                'color' => 'black',
                'symbol' => '\U2663',
                'face' => '2',
                'strength' => 0
            ],
            [
                'suite' => 'hearts',
                'color' => 'red',
                'symbol' => '\U2665',
                'face' => '3',
                'strength' => 0
            ]
        ];

        $exposedCard = [
            'suite' => 'spades',
            'color' => 'black',
            'symbol' => '\U2663',
            'face' => '2',
            'strength' => 0
        ];

        $player1 = new Player();

        // feed the cards to player
        $player1->setDeck($cards);

        $playerDeck = $player1->getDeck();
        $this->assertEquals(4, count($playerDeck));

        $strengthCards = $player1->updateCardStrength($playerDeck, $exposedCard);

        // feed the cards to player
        $player1->setDeck($strengthCards);

        $playedCard  = $player1->playCard();

        $this->assertEquals(2, $playedCard['strength']);
        $this->assertEquals('clubs', $playedCard['suite']);
        $this->assertEquals('black', $playedCard['color']);
    }
}   