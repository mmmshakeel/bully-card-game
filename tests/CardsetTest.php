<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Cardset;
use App\Player;

final class CardsetTest extends TestCase
{
    public function testShouldHave52CardsWithoutJoker()
    {
        $cardSet = new Cardset();
        $this->assertEquals(52, count($cardSet->getCards()));
    }

    public function testShuffleCards()
    {
        $cardSet = new Cardset();

        $cardSet->shuffle();
        $this->assertEquals(52, count($cardSet->getCards()));
    }

    public function testDivideCards()
    {
        $cardSet = new Cardset();

        $cardSet->shuffle();

        $player1 = new Player();
        $player2 = new Player();
        $player3 = new Player();
        $player4 = new Player();

        $remainDeck = $cardSet->divideCards([$player1, $player2, $player3, $player4], 7);
        $this->assertEquals(24, count($remainDeck));

        $this->assertEquals(7, count($player1->getDeck()));
        $this->assertEquals(7, count($player2->getDeck()));
        $this->assertEquals(7, count($player3->getDeck()));
        $this->assertEquals(7, count($player4->getDeck()));
    }

    public function testExposeCard()
    {
        $cardSet = new Cardset();

        $card1 = [
            'suite' => 'hearts',
            'color' => 'red',
            'symbol' => '\U2665',
            'face' => '2',
            'strength' => 0
        ];
        $card2 = [
            'suite' => 'clubs',
            'color' => 'black',
            'symbol' => '\U2663',
            'face' => '3',
            'strength' => 0
        ];

        $remainDeck = [$card1, $card2];
        $cardSet->setRemainDeck($remainDeck);
        $exposedCard = $cardSet->flipCard();

        $this->assertEquals('hearts', $exposedCard['suite']);
    }

}