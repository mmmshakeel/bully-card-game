<?php

require __DIR__ . '/vendor/autoload.php';

use App\Cardset;
use App\Player;

// start the game
$cardset = new Cardset();

// initiate players
$player1 = new Player();
$player2 = new Player();
$player3 = new Player();
$player4 = new Player();

// set the names
$player1->setName('Freek');
$player2->setName('Bas');
$player3->setName('Henk');
$player4->setName('Pieter');

print("\nStarting game with " . $player1->getName() . ", " . $player2->getName() . ", " . $player3->getName() . ", " . $player4->getName() . "\n\n");

// shuffle the cards
$cardset->shuffle();

// divide the cards
$cardset->divideCards([$player1, $player2, $player3, $player4], 7);

// player1 cards
$player1Deck = $player1->getDeck();
print($player1->getName() . " has been dealt: ");
foreach ($player1Deck as $card) {
    print($card['symbol'] . " " . $card['face'] . "   ");
}
print("\n");

// player2 cards
$player2Deck = $player2->getDeck();
print($player2->getName() . " has been dealt: ");
foreach ($player2Deck as $card) {
    print($card['symbol'] . " " . $card['face'] . "   ");
}
print("\n");

// player3 cards
$player3Deck = $player3->getDeck();
print($player3->getName() . " has been dealt: ");
foreach ($player3Deck as $card) {
    print($card['symbol'] . " " . $card['face'] . "   ");
}
print("\n");

// player4 cards
$player4Deck = $player4->getDeck();
print($player4->getName() . " has been dealt: ");
foreach ($player4Deck as $card) {
    print($card['symbol'] . " " . $card['face'] . "   ");
}
print("\n");

// set the remaining deck
$remainingDeck = $cardset->getCards();
// $cardset->setRemainDeck($remainingDeck);
$topCard = $cardset->flipCard();

print("\nTop card is " . $topCard['symbol'] . " " . $topCard['face'] . "\n\n");

while (count($player1Deck) != 0 || count($player2Deck) != 0 || count($player3Deck) != 0 || count($player4Deck) != 0) {

    print("\n");

    // make sure the remaining deck is correct
    $remainingDeck = $cardset->getCards();

    // ================= Player 1 ================= //
    $player1Deck = $player1->getDeck();
    if ($player1->isWon()) {
        print($player1->getName() . " has won. \n\n");
        break;
    }

    // reset any strengths
    $player1Deck = $player1->resetCardStrength($player1Deck);

    // check the strength based on the top card
    $player1Deck = $player1->updateCardStrength($player1Deck, $topCard);
    $player1->setDeck($player1Deck);

    $tempTopCard = $player1->playCard();
    if ($tempTopCard['strength'] == 0) {
        // put back the top card to players deck
        $player1->getCard($tempTopCard);
        $remainingDeck = $player1->getCardfromRemainDeck($remainingDeck);
        $cardset->setRemainDeck($remainingDeck);
        $takenCard = $player1->getTakenCard();
        print($player1->getName() . " does not have suitable card, taking from deck " . $takenCard['symbol'] . " " . $takenCard['face'] . "\n");
    } else {
        $topCard = $tempTopCard;
        print($player1->getName() . " plays " . $topCard['symbol'] . " " . $topCard['face'] . "\n");

        if ($player1->isWon()) {
            print($player1->getName() . " has won. \n\n");
            break;
        }
    }

    // ================= Player 2 ================= //
    $player2Deck = $player2->getDeck();
    if ($player2->isWon()) {
        print($player2->getName() . " has won. \n\n");
        break;
    }

    // reset any strengths
    $player2Deck = $player2->resetCardStrength($player2Deck);

    // check the strength based on the top card
    $player2Deck = $player2->updateCardStrength($player2Deck, $topCard);
    $player2->setDeck($player2Deck);
    
    $tempTopCard = $player2->playCard();
    if ($tempTopCard['strength'] == 0) {
        // put back the top card to players deck
        $player2->getCard($tempTopCard);
        $remainingDeck = $player2->getCardfromRemainDeck($remainingDeck);
        $cardset->setRemainDeck($remainingDeck);
        $takenCard = $player2->getTakenCard();
        print($player2->getName() . " does not have suitable card, taking from deck " . $takenCard['symbol'] . " " . $takenCard['face'] . "\n");
    } else {
        $topCard = $tempTopCard;
        print($player2->getName() . " plays " . $topCard['symbol'] . " " . $topCard['face'] . "\n");

        if ($player2->isWon()) {
            print($player2->getName() . " has won. \n\n");
            break;
        }
    }

    // ================= Player 3 ================= //
    $player3Deck = $player3->getDeck();
    if ($player3->isWon()) {
        print($player3->getName() . " has won. \n\n");
        break;
    }

    // reset any strengths
    $player3Deck = $player3->resetCardStrength($player3Deck);

    // check the strength based on the top card
    $player3Deck = $player3->updateCardStrength($player3Deck, $topCard);
    $player3->setDeck($player3Deck);
    
    $tempTopCard = $player3->playCard();
    if ($tempTopCard['strength'] == 0) {
        // put back the top card to players deck
        $player3->getCard($tempTopCard);
        $remainingDeck = $player3->getCardfromRemainDeck($remainingDeck);
        $cardset->setRemainDeck($remainingDeck);
        $takenCard = $player3->getTakenCard();
        print($player3->getName() . " does not have suitable card, taking from deck " . $takenCard['symbol'] . " " . $takenCard['face'] . "\n");
    } else {
        $topCard = $tempTopCard;
        print($player3->getName() . " plays " . $topCard['symbol'] . " " . $topCard['face'] . "\n");

        if ($player3->isWon()) {
            print($player3->getName() . " has won. \n\n");
            break;
        }
    }

    // ================= Player 4 ================= //
    $player4Deck = $player4->getDeck();
    if ($player4->isWon()) {
        print($player4->getName() . " has won. \n\n");
        break;
    }

    // reset any strengths
    $player4Deck = $player4->resetCardStrength($player4Deck);

    // check the strength based on the top card
    $player4Deck = $player4->updateCardStrength($player4Deck, $topCard);
    $player4->setDeck($player4Deck);

    $tempTopCard = $player4->playCard();
    if ($tempTopCard['strength'] == 0) {
        // put back the top card to players deck
        $player4->getCard($tempTopCard);
        $remainingDeck = $player4->getCardfromRemainDeck($remainingDeck);
        $cardset->setRemainDeck($remainingDeck);
        $takenCard = $player4->getTakenCard();
        print($player4->getName() . " does not have suitable card, taking from deck " . $takenCard['symbol'] . " " . $takenCard['face'] . "\n");
    } else {
        $topCard = $tempTopCard;
        print($player4->getName() . " plays " . $topCard['symbol'] . " " . $topCard['face'] . "\n");

        if ($player4->isWon()) {
            print($player4->getName() . " has won. \n\n");
            break;
        }
    }
}
