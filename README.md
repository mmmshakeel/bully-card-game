Simplified Bully Card Game
===========================

This project has deployed in docker environment. Below instruction are assuming you have docker and docker-compose installed in your local machine.

1. go to the project folder 
`cd bully-card-game`

2. run command 
`docker-compose up -d --build`

3. run 
`docker-compose exec app composer install`

#### Run unit tests
`docker-compose exec app vendor/bin/phpunit`

#### Play the game
`docker-compose exec app php play.php`

