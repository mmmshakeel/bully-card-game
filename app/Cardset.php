<?php

namespace App;

class Cardset
{
    // card suite attributes
    private $cardSuits = [
        [
            'suite' => 'clubs',
            'color' => 'black',
            'symbol' => '♣'
        ],
        [
            'suite' => 'diamonds',
            'color' => 'red',
            'symbol' => '♦'
        ],
        [
            'suite' => 'hearts',
            'color' => 'red',
            'symbol' => '♥'
        ],
        [
            'suite' => 'spades',
            'color' => 'black',
            'symbol' => '♠'
        ]
    ];

    // card faces
    private $faces = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'King', 'Queen'];

    private $cards;

    private $exposedCard;

    public function __construct()
    {
        $this->cards = $this->createCards($this->cardSuits, $this->faces);
    }

    /**
     * Create the card pack
     *
     * @param array $cardSuits
     * @param array $faces
     * @return array  The cards 
     */
    public function createCards($cardSuits, $faces)
    {
        $cardsArray = [];

        foreach ($cardSuits as $item) {
            foreach ($faces as $face) {
                $cardsArray[] = [
                    'suite' => $item['suite'],
                    'color' => $item['color'],
                    'symbol' => $item['symbol'],
                    'face' => $face,
                    'strength' => 0
                ];
            }
        }

        return $cardsArray;
    }

    /**
     * Shuffle the cards
     *
     * @return void
     */
    public function shuffle() 
    {
        shuffle($this->cards);
    }

    /**
     * Get the cards, which are not given to player's
     * This can be used as the left over deck
     *
     * @return void
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * Devide the cards between each player
     *
     * @param array $players    Array of players
     * @param int $cardCount    Number of cards for each player
     * @return array    Returns the left over cards 
     */
    public function divideCards($players, $cardCount)
    {
        for ($x = 0; $x < $cardCount; $x++) {
            foreach ($players as $player) {
                $player->getCard(array_shift($this->cards));
            }
        }

        return $this->cards;
    }

    /**
     * Set the left over cards
     *
     * @param array $cards  List of card arrays
     * @return void
     */
    public function setRemainDeck($cards)
    {
        $this->cards = $cards;
    }

    /**
     * Flip the card to start the game
     *
     * @return array    Return the flipped card
     */
    public function flipCard()
    {
        $this->exposedCard = array_shift($this->cards);
        return $this->exposedCard;
    }

    
}