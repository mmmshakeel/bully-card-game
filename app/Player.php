<?php
/**
 * Contains classes of the player
 * 
 * @author Shakeel Mohamed <mmmshakeel@gmail.com>
 */

namespace App;

class Player
{
    private $name;
    
    private $cards = [];

    private $takenCard;

    /**
     * Set the name of the plater
     *
     * @param string $name  Plaayer name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the name of the player
     *
     * @return string   Player name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Player takes a card and add it to his deck
     *
     * @param array  $card  The card array
     * @return array    The array of cards of the deck
     */
    public function getCard($card)
    {
        array_unshift($this->cards, $card);

        return $this->cards;
    }
    
    /**
     * Player plays a card. Removes the card from his deck
     * 
     * This method sorts the deck based on the strngth at the given moment.
     * And the most strengthned card will be played
     *
     * @return array The played/removed card
     */
    public function playCard()
    {
        usort($this->cards, function($a, $b) {
            return $a['strength'] - $b['strength'];
        });

        $this->cards = array_reverse($this->cards);
        return array_shift($this->cards);
    }

    /**
     * Get the players card deck
     *
     * @return array    Card deck array of the player
     */
    public function getDeck()
    {
        return $this->cards;
    }

    /**
     * Set the players card deck
     *
     * @param array $cards
     * @return void
     */
    public function setDeck($cards)
    {
        $this->cards = $cards;
    }

    /**
     * Player takes a card from the left over deck
     * 
     * Removes the taken card from the left over deck
     *
     * @param array $remainDeck     List of cards as the left over desk 
     * @return void
     */
    public function getCardfromRemainDeck($remainDeck)
    {
        $this->takenCard = array_shift($remainDeck);
        $this->getCard($this->takenCard);
        return $remainDeck;
    }

    /**
     * Get the card taken from the left over deck
     *
     * @return array    The card array
     */
    public function getTakenCard()
    {
        return $this->takenCard;
    }

    /**
     * Reset the calculated strength of each card
     * 
     * Use this function to reset the strength before each move 
     * to decide which card to play
     *
     * @param array $cards
     * @return array  Strength resetted players card deck
     */
    public function resetCardStrength($cards)
    {
        $resettedCards = [];
        foreach ($cards as $card) {
            $tempCard = [];
            foreach ($card as $key => $value) {
                if ($key == 'strength') {
                    $value = 0;
                    $tempCard[$key] = $value;
                } else {
                    $tempCard[$key] = $value;
                }
            }

            $resettedCards[] = $tempCard;
        }

        return $resettedCards;
    }

    /**
     * Gives a strength score to each card of the player's deck based on the exposed/top card
     * 
     * @param array $cards  Player's deck of cards
     * @param array $exposedCard    The top card
     * @return array    Strength score calculated player's card deck
     */
    public function updateCardStrength($cards, $exposedCard)
    {
        $tempCards = [];

        foreach ($cards as $card) {
            $tempCard = [];
            $suiteMatch = 0;
            $colorMatch = 0;
            $faceMatch = 0;

            foreach ($card as $key => $value) {
                if ($key == 'suite' && $value == $exposedCard['suite']) {
                    $suiteMatch = 1;
                }

                if ($key == 'color' && $value == $exposedCard['color']) {
                    $colorMatch = 1;
                }

                if ($key == 'face' && $value == $exposedCard['face']) {
                    $faceMatch = 1;
                }

                if ($key == 'strength') {
                    $tempCard[$key] = $suiteMatch + $colorMatch + $faceMatch;
                } else {
                    $tempCard[$key] = $value;
                }
            }
            $tempCards[] = $tempCard;
        }

        return $tempCards;
    }

    /**
     * Determine if the player won the game. 
     * Player wins when there are no card remaining the player's deck
     *
     * @return boolean
     */
    public function isWon()
    {
        return (count($this->cards) == 0) ? true : false;
    }
}
